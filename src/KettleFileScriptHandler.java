package com.newtec.dataprocess.executor.jobhandler;

import cn.hutool.core.util.StrUtil;

import com.newtec.dataprocess.core.context.XxlJobHelper;
import com.newtec.dataprocess.core.handler.IJobHandler;
import com.newtec.dataprocess.core.handler.annotation.XxlJob;
import com.newtec.dataprocess.executor.model.KettleFileScriptParam;
import com.newtec.dataprocess.executor.model.MyJobListener;
import com.newtec.dataprocess.executor.model.MyTransListener;
import com.newtec.dataprocess.kettle.common.utils.KettleParamUtils;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author: kettle 脚本模式执行器
 * @date: 2020-09-25 10:21
 * @version: 1.0
 * @description: 功能描述
 */
@Component
public class KettleFileScriptHandler extends IJobHandler {
    /**
     * execute handler, invoked when executor receives a scheduling request
     *
     * @return
     * @throws Exception
     */
    @XxlJob(value = "fileScriptHandler", init = "init", destroy = "destroy")
    @Override
    public void execute() throws Exception {
        String param = XxlJobHelper.getJobParam();
        if (StrUtil.isEmpty(param)) {
            XxlJobHelper.log("请参照执行器管理的参数格式");
            XxlJobHelper.handleFail("请参照执行器管理的参数格式");
            return;
        }
        KettleFileScriptParam fileScriptParam = KettleFileScriptParam.toParam(param);
        if (fileScriptParam == null) {
            XxlJobHelper.log("请参照执行器管理的参数格式");
            XxlJobHelper.handleFail("请参照执行器管理的参数格式");
            return;
        }
        //判断参数是否正确
        if (StrUtil.isEmpty(fileScriptParam.getRootPath())) {
            XxlJobHelper.log("根路径不能为空");
            XxlJobHelper.handleFail("根路径不能为空");
            return;
        }
        if (StrUtil.isEmpty(fileScriptParam.getRelativePath())) {
            XxlJobHelper.log("脚本相对路径不能为空");
            XxlJobHelper.handleFail("脚本相对路径不能为空");
            return;
        }
        //check script exit
        String filePath = fileScriptParam.getRootPath() + fileScriptParam.getRelativePath();
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                XxlJobHelper.log("脚本不存在");
                XxlJobHelper.handleFail("脚本不存在");
                return;
            }
        } catch (Exception e) {
            XxlJobHelper.log(e);
            e.printStackTrace();
            XxlJobHelper.log("脚本不存在");
            XxlJobHelper.handleFail("脚本不存在");
            return;
        }

        try {
            //执行转换或者job
            if (filePath.contains(".ktr")) {
                TransMeta transMeta = new TransMeta(filePath);
                Trans trans = new Trans(transMeta);
                trans.addTransListener(new MyTransListener());
                trans.execute(null);
                //设置参数
                KettleParamUtils.setTransParams(trans, fileScriptParam.getParams());
                trans.waitUntilFinished();
                if (trans.getErrors() > 0) {
                    XxlJobHelper.log("脚本执行报错！！");
                    XxlJobHelper.handleFail("脚本执行报错！！");
                    return;
                }
            } else if (fileScriptParam.getRelativePath().contains(".kjb")) {
                JobMeta jobMeta = new JobMeta(filePath, null);
                Job job = new Job(null, jobMeta);
                job.addJobListener(new MyJobListener());
                //设置参数
                KettleParamUtils.setJobParams(job, jobMeta, fileScriptParam.getParams());
                job.run();
                job.waitUntilFinished(); // 等待转换执行结束
                if (job.getErrors() > 0) {
                    XxlJobHelper.log("脚本不是可执行脚本");
                    XxlJobHelper.handleFail("脚本不是可执行脚本！！");
                    return;
                }
            } else {
                XxlJobHelper.log("脚本不是可执行脚本");
                XxlJobHelper.handleFail("脚本不是可执行脚本！！");
                return;
            }

        } catch (KettleException e) {
            e.printStackTrace();
            XxlJobHelper.handleFail("kettle 脚本执行报错，请检查脚本！！");
            return;
        }
        XxlJobHelper.handleSuccess("执行成功！！");
    }


    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void destroy() throws Exception {
        super.destroy();
    }
}
