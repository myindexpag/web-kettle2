package com.newtec.dataprocess.executor.service;


import com.newtec.dataprocess.executor.model.WebEtlParam;
import com.newtec.dataprocess.kettle.model.TransJobInfo;

/**
 * @author: GM
 * @date: 2021-03-09 17:54
 * @version: 1.0
 * @description: 功能描述
 */
public interface IJobTransExecutor {

    void doRunJob(WebEtlParam webEtlParam, TransJobInfo transJobInfo);
    void doRunTrans(WebEtlParam webEtlParam, TransJobInfo transJobInfo);
}
